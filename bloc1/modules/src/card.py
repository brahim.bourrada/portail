
import random as alea
import builtins

VALUES = ("as", "2", "3", "4", "5", "6", "7", "8", "9", "10", "valet", "dame", "roi")
"""
tuple of possible values    
"""
COLORS = ("trèfle", "coeur", "carreau", "pique")
"""
tuple of possible colors
"""


def create(value, color):
    """
    creates a card with value and color

    :param value: value of the card
    :type value: element of `VALUES`
    :param color: color of the card
    :type color: element of `COLORS`
    :return: the created card
    :rtype: a dictionnary 
    """
    assert type(value) == builtins.str and value.lower() in VALUES,\
           "first argument must be one of " + builtins.str(VALUES)
    assert type(color) == builtins.str and color.lower() in COLORS,\
           "second argument must be one of " + builtins.str(COLORS)
    return { 'color' : color, 'value' : value }

def get_color(card):
    """
    returns the color of the card

    :return: the color of the card
    :rtype: str
    :UC: none
    :Example:

    >>> card = create('as', 'coeur')
    >>> get_color(card) == 'coeur'
    True
    """
    return card['color']

def get_value(card):
    """
    returns the value of the card

    :param card: the card
    :type card: card
    :returns:  the value of the card
    :rtype: str
    :UC: none
    :Example:
    
    >>> card = create('as', 'coeur')
    >>> get_value(card)
    'as'
    """
    return card['value']


def compare_value(card_1, card_2):
    """
    compares card colors, returns positive, negative or zero value
    
    :param card: the second card
    :type card: card
    :returns: *(int)* --      
      
       * a positive number if card_1's color is greater than card_2's
       * a negative number if card_1's color is lower than card_2's
       * 0 if card_1's color is the same greater than card_2's
    :UC: none
    :Example: 

    >>> c1 = create('as', 'coeur')
    >>> c2 = create('roi', 'coeur')
    >>> c3 = create('as', 'pique')
    >>> compare_value(c1, c2) < 0
    True
    >>> compare_value(c2, c1) > 0
    True
    >>> compare_value(c1, c3) == 0
    True
    """    
    return VALUES.index(get_value(card_1)) - VALUES.index(get_value(card_2))

def compare_color(card_1, card_2):
    """
    compares card colors, returns positive, negative or zero value
    
    :param card: the second card
    :type card: card
    :returns: *(int)* --      
      
       * a positive number if card_1's color is greater than card_2's
       * a negative number if card_1's color is lower than card_2's
       * 0 if card_1's color is the same greater than card_2's
    :UC: none
    :Example: 

    >>> c1 = create('as', 'coeur')
    >>> c2 = create('roi', 'coeur')
    >>> c3 = create('as', 'pique')
    >>> compare_color(c1, c3) < 0
    True
    >>> compare_color(c3, c1) > 0
    True
    >>> compare_color(c1, c2) == 0
    True
    """    
    return COLORS.index(get_color(card_1)) - COLORS.index(get_color(card_2))

def compare(card_1, card_2):
    """
    compares card colors, returns positive, negative or zero value
    
    :param card: the second card
    :type card: card
    :returns: *(int)* --      
      
       * a positive number if card_1's color is greater than card_2's
       * a negative number if card_1's color is lower than card_2's
       * 0 if card_1's color is the same greater than card_2's
    :UC: none
    :Example: 

    >>> c1 = create('as', 'coeur')
    >>> c2 = create('roi', 'coeur')
    >>> c3 = create('as', 'pique')
    >>> c1bis = create('as', 'coeur')
    >>> compare(c1, c2) < 0
    True
    >>> compare(c2, c1) > 0
    True
    >>> compare(c1, c3) < 0
    True
    >>> compare(c1, c1bis) == 0
    True
    """     
    cmp_values = compare_value(card_1,card_2)
    if cmp_values == 0:
        return compare_color(card_1,card_2)
    else:
        return cmp_values

def equals(card_1,card_2):
    """
    returns true if both cards are equals, false else
    
    :param card: the second card
    :type card: card
    :returns: *(boolean)* true if both *card_1* and *card_2* are equals, false else
    :UC: none
    :Example: 

    >>> c1 = create('as', 'coeur')
    >>> c2 = create('roi', 'coeur')
    >>> c1bis = create('as', 'coeur')
    >>> equals(c1, c2) 
    False
    >>> equals(c1, c1bis)
    True
    """  
    return compare(card_1,card_2) == 0



def random():
    """
    create a card whose value and color are randomly chosen
    
    :returns: *(card)* -- a randomly chosen card
    
    >>> c = random()
    >>> get_value(c) in VALUES
    True
    >>> get_color(c) in COLORS
    True
    """
    value = alea.randrange(len(VALUES))
    color = alea.randrange(len(COLORS))
    return create(VALUES[value], COLORS[color])

def str(card):
    return '{} de {}'.format(get_value(card).upper(), get_color(card))

def print(card, end='\n'):
    builtins.print(str(card), end=end)

#===========================================

def main():
    some_card = create('10','coeur')
    print(some_card)
    other_card = random()
    print(other_card)
    builtins.print('comparaison {} et {} = {}'.format(str(some_card), str(other_card),compare(some_card,other_card)))

if __name__ == '__main__':
    import doctest
    doctest.testmod()
    main()
