Travail à rendre, blocs 1-3
---------------------------

→ Comment
---------

L'ensemble des travaux est à rendre vis des dépôts sur le GitLab.  
Créez vos dépôts et invitez Benoit `@benoit.papegay` et Philippe `@philippe.marquet` comme membre de vos projets. 

→ Quand
-------

**Aujourd'hui 6 juillet**. 
Travail bloc 1 en l'état à l'issue du TP Wator. 

**Avant les prochains jours de formations**. 
L'ensemble des travaux sont à rendre avant les prochains jours de formations.  
C'est-à-dire semaine du 8 nov. ou du 13 décembre 2021.

→ Quoi
------

### Bloc 1 ###

Rendre votre travail sur Wator. Veiller à respecter les points
  suivants :

- code documenté au moyen de docstring
- code modulaire : réaliser un module par structure de données
  utilisée
- dans le `Readme.md` du projet, citer les points du programme de
  NSI en lien avec le sujet et situer l'activité dans une progression
  NSI (pré-requis, modalités)

### Bloc 2 ###

Algorithmes glouton

- identifier et décrire avec précision (comme on le ferait pour un
projet) un problème qui peut se résoudre par un algorithme glouton

### Bloc 3 ###

Réseau, shell

- proposer une activité autour du réseaux, et éventuellement du shell,
  physique (Raspeberry ou autre) ou virtuelle (avec un logiciel de
  simulation comme Filius).  
  Cette activité mettra en évidence la mise à jour des tables
  de routage lorsque l'on utilise plusieurs routeurs (ajout ou
  suppression de routeurs par exemple).
